using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modulus.Processes.Entities;
using Modulus.Processes.Processors;
using Modulus.Processes.Processors.Implementations;

namespace Modulus.Processes.Test
{
    [TestClass]
    public sealed class ModulusProcessorTest
    {
        IModulusProcessor modProcessor = new ModulusProcessor();

        [TestMethod]
        public void Mod10()
        {
            var accountDetails = new AccountDetails
            {
                SortCode = "089999",
                AccountNumber = "66374958",
            };

           var result = modProcessor.Validate(accountDetails);
            Assert.IsTrue(result.IsValid);
        }

        [TestMethod]
        public void Mod11()
        {
            var accountDetails = new AccountDetails
            {
                SortCode = "107999",
                AccountNumber = "88837491",
            };

            var result = modProcessor.Validate(accountDetails);
            Assert.IsTrue(result.IsValid);
        }
        
        [TestMethod]
        public void DblAl()
        {
            var accountDetails = new AccountDetails
            {
                SortCode = "041399",
                AccountNumber = "33748472",
            };

            var result = modProcessor.Validate(accountDetails);
            Assert.IsTrue(result.IsValid);
        }
    }
}
