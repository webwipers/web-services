﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modulus.DataAccessLayer.Entities;
using Modulus.Processes.Functions;
using System.Collections.Generic;
using TargetClass = Modulus.Processes.Functions.Implementations.AlternateFunction;

namespace Modulus.Processes.Test.Functions
{
    [TestClass]
    public sealed class AlternateFunction
    {

        [TestMethod]
        public void DblAl()
        {
            IModFunction modFunction = new TargetClass();

            var weights = new WeightTableEntry()
            {
                StartIndex = 000000,
                EndIndex = 000000,
                ModCheck = DataAccessLayer.Enums.CheckType.DBLAL,
                ValueDictionary = new Dictionary<char, int>
                {
                    { 'u', 0 },
                    { 'v', 0 },
                    { 'w', 0 },
                    { 'x', 0 },
                    { 'y', 0 },
                    { 'z', 0 },
                    { 'a', 0 },
                    { 'b', 0 },
                    { 'c', 0 },
                    { 'd', 0 },
                    { 'e', 0 },
                    { 'f', 0 },
                    { 'g', 2 },
                    { 'h', 1 },
                },
            };

            var result = modFunction.PerformCheck("00000000000075", weights);

            Assert.IsTrue(result.Passes);
        }
    }
}
