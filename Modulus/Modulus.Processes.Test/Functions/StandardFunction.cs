﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modulus.DataAccessLayer.Entities;
using Modulus.Processes.Functions;
using System.Collections.Generic;
using TargetClass = Modulus.Processes.Functions.Implementations.StandardFunction;

namespace Modulus.Processes.Test.Functions
{
    [TestClass]
    public sealed class StandardFunction
    {

        [TestMethod]
        public void Mod10()
        {
            IModFunction modFunction = new TargetClass(10);

            var weights = new WeightTableEntry()
            {
                StartIndex = 000000,
                EndIndex = 000000,
                ModCheck = DataAccessLayer.Enums.CheckType.MOD10,
                ValueDictionary = new Dictionary<char, int>
                {
                    { 'u', 0 },
                    { 'v', 0 },
                    { 'w', 0 },
                    { 'x', 0 },
                    { 'y', 0 },
                    { 'z', 0 },
                    { 'a', 0 },
                    { 'b', 0 },
                    { 'c', 0 },
                    { 'd', 0 },
                    { 'e', 0 },
                    { 'f', 0 },
                    { 'g', 10 },
                    { 'h', 10 },
                },
            };

            var result = modFunction.PerformCheck("00000000000111", weights);

            Assert.IsTrue(result.Passes);
        }

        [TestMethod]
        public void Mod11()
        {
            IModFunction modFunction = new TargetClass(11);

            var weights = new WeightTableEntry()
            {
                StartIndex = 000000,
                EndIndex = 000000,
                ModCheck = DataAccessLayer.Enums.CheckType.MOD11,
                ValueDictionary = new Dictionary<char, int>
                {
                    { 'u', 0 },
                    { 'v', 0 },
                    { 'w', 0 },
                    { 'x', 0 },
                    { 'y', 0 },
                    { 'z', 0 },
                    { 'a', 0 },
                    { 'b', 0 },
                    { 'c', 0 },
                    { 'd', 0 },
                    { 'e', 0 },
                    { 'f', 0 },
                    { 'g', 11 },
                    { 'h', 11 },
                },
            };

            var result = modFunction.PerformCheck("00000000000111", weights);

            Assert.IsTrue(result.Passes);
        }
    }
}
