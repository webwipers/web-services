using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modulus.DataAccessLayer.DataAccessors;
using System.Linq;
using TargetClass = Modulus.DataAccessLayer.DataAccessors.Implementations.FileDataAccessor;

namespace Modulus.DataAccessLayer.Test
{
    [TestClass]
    public class FileDataAccessor
    {

        [TestMethod]
        public void GetFileContents()
        {
            IFileDataAccessor fileAccessor = new TargetClass();

            var tableData = fileAccessor.WeightTable;

            var sortCode = 779414;

            var rows = tableData.Where(r => r.StartIndex <= sortCode &&  r.EndIndex >= sortCode);

            Assert.IsTrue(rows.Any());
            Assert.AreEqual(1, rows.Count());

            var row = rows.First();
            Assert.AreEqual(Enums.CheckType.MOD11, row.ModCheck);
            Assert.AreEqual(0, row.U);
            Assert.AreEqual(0, row.V);
            Assert.AreEqual(1, row.W);
            Assert.AreEqual(2, row.X);
            Assert.AreEqual(5, row.Y);
            Assert.AreEqual(3, row.Z);
            Assert.AreEqual(6, row.A);
            Assert.AreEqual(4, row.B);
            Assert.AreEqual(8, row.C);
            Assert.AreEqual(7, row.D);
            Assert.AreEqual(10, row.E);
            Assert.AreEqual(9, row.F);
            Assert.AreEqual(3, row.G);
            Assert.AreEqual(1, row.H);
            Assert.AreEqual(7, row.Ex);

        }


        [TestMethod]
        public void duplicates()
        {
            IFileDataAccessor fileAccessor = new TargetClass();

            var tableData = fileAccessor.WeightTable;

            var sortCode = 779414;

            var rows = tableData.Where(r => r.StartIndex <= sortCode && r.EndIndex >= sortCode);

            Assert.IsTrue(rows.Any());
            Assert.AreEqual(1, rows.Count());

            var row = rows.First();
            Assert.AreEqual(Enums.CheckType.MOD11, row.ModCheck);
            Assert.AreEqual(0, row.U);
            Assert.AreEqual(0, row.V);
            Assert.AreEqual(1, row.W);
            Assert.AreEqual(2, row.X);
            Assert.AreEqual(5, row.Y);
            Assert.AreEqual(3, row.Z);
            Assert.AreEqual(6, row.A);
            Assert.AreEqual(4, row.B);
            Assert.AreEqual(8, row.C);
            Assert.AreEqual(7, row.D);
            Assert.AreEqual(10, row.E);
            Assert.AreEqual(9, row.F);
            Assert.AreEqual(3, row.G);
            Assert.AreEqual(1, row.H);
            Assert.AreEqual(7, row.Ex);

        }
    }
}
