﻿using Microsoft.AspNetCore.Mvc;
using Modulus.Processes.Entities;
using Modulus.Processes.Processors;
using Modulus.Processes.Processors.Implementations;

namespace Modulus.Web.Controllers
{
    [Route("Account")]
    public sealed class AccountController : Controller
    {
        private readonly IModulusProcessor _modulusProcessor = new ModulusProcessor();

        [HttpGet]
        [Route("Validate")]
        public IActionResult Validate([FromQuery] string sortCode, [FromQuery] string accountNumber)
        {
            var accountDetails = new AccountDetails
            {
                SortCode = sortCode,
                AccountNumber = accountNumber
            };

            var result = _modulusProcessor.Validate(accountDetails);

            // call logic
            return new OkObjectResult(result);
        }
        
    }
}
