﻿namespace Modulus.Processes.Entities
{
    public sealed class ModResult
    {
        public int ResultSum { get; set; }

        public int Remainder { get; set; }

        public bool Passes { get; set; }
    }
}
