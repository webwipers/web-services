﻿namespace Modulus.Processes.Entities
{
    public sealed class AccountDetails
    {
        public string AccountNumber { get; set; }

        public string SortCode { get; set; }


    }
}
