﻿using Modulus.DataAccessLayer.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Modulus.Processes.Entities
{
    public sealed class ValidationResult
    {
        public ValidationResult()
        {
            Exceptions = new List<int>();
        }

        public bool IsValid { get; set; }

        public string Message { get; set; }

        public CheckType CheckType { get;set;}

        public List<int> Exceptions { get; set; }

        public ModResult ModulusResult { get; set; }
    }
}
