﻿using Modulus.DataAccessLayer.Repositories;
using Modulus.DataAccessLayer.Repositories.Implementations;
using Modulus.Processes.Entities;
using Modulus.Processes.Functions;
using System.Linq;
using System.Text.RegularExpressions;

namespace Modulus.Processes.Processors.Implementations
{
    public sealed class ModulusProcessor : IModulusProcessor
    {
        private static readonly Regex SortCodeRegex = new Regex("^[0-9]{6}$", RegexOptions.Compiled);
        private static readonly Regex AccountNumberRegex = new Regex("^[0-9]{8}$", RegexOptions.Compiled);

        private readonly IWeightTableRepository _weightTableRepository = new WeightTableRepository();   
        
        ValidationResult IModulusProcessor.Validate(AccountDetails accountDetails)
        {
            var validationResult = new ValidationResult();

            // Regex Check Format
            if(!SortCodeRegex.IsMatch(accountDetails.SortCode) || !AccountNumberRegex.IsMatch(accountDetails.AccountNumber))
            {
                validationResult.IsValid = true;
                validationResult.Message = "No check possible for the sort code and account number of this format/length. ";
                return validationResult;
            }

            // Weight Table Lookup
            var weights = _weightTableRepository.GetWeightsForSortCode(accountDetails.SortCode);
            if(!weights.Any())
            {
                validationResult.IsValid = true;
                validationResult.Message = "No check found for SortCode. Assumed to be valid as per Section 2 of the Specification. ";
                return validationResult;
            }

            if(weights.Count() > 1)
            {
                validationResult.Exceptions.AddRange(weights.Select(w => w.Ex ?? 0).Distinct());
            }

            var weight = weights.First();

            //Check For Exceptions
            validationResult.CheckType = weight.ModCheck;

            switch(weight.Ex)
            {
                case 4:
                case 7:
                case null:
                    break;
                default:
                    validationResult.IsValid = true;
                    validationResult.Exceptions.Add(weight.Ex.Value);
                    validationResult.Message = $"Exception(s) {string.Join(',', validationResult.Exceptions.ToArray())} can not be handled. ";
                    return validationResult;
            }

            //Perform Modulus
            var accountDetailString = $"{accountDetails.SortCode}{accountDetails.AccountNumber}";

            var modFunction = Factory.GetModFunction(weight.ModCheck);

            var funcitonResult = modFunction.PerformCheck(accountDetailString, weight);

            //Return Results
            validationResult.IsValid = funcitonResult.Passes;
            validationResult.ModulusResult = funcitonResult;            
            return validationResult;
        }
        
    }
}
