﻿using Modulus.Processes.Entities;

namespace Modulus.Processes.Processors
{
    public interface IModulusProcessor
    {
        ValidationResult Validate(AccountDetails accountDetails);
    }
}
