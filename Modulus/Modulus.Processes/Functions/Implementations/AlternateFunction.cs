﻿using System.Linq;
using Modulus.DataAccessLayer.Entities;
using Modulus.Processes.Entities;

namespace Modulus.Processes.Functions.Implementations
{
    public sealed class AlternateFunction : BaseModFunction, IModFunction
    {
        private const int ModValue = 10;

        ModResult IModFunction.PerformCheck(string accountNumber, WeightTableEntry weights)
        {
            var positionValues = GetPositionValues(accountNumber);

            var multiples = positionValues
                .Select(v => weights.ValueDictionary
                    .First(w => w.Key == v.Key)
                    .Value * v.Value
                );

            var sum = multiples
                .SelectMany(m => GetIntsFromString(m.ToString()))
                .Sum();

            var remainder = sum % ModValue;

            var result = new ModResult
            {
                ResultSum = sum,
                Remainder = remainder,
                Passes = remainder == 0,
            };

            return result;
        }
    }
}
