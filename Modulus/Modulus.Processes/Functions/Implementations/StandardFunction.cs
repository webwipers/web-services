﻿using Modulus.DataAccessLayer.Entities;
using Modulus.Processes.Entities;
using System.Linq;

namespace Modulus.Processes.Functions.Implementations
{
    public sealed class StandardFunction : BaseModFunction, IModFunction
    {
        private int ModValue { get; }

        public StandardFunction(int modValue)
        {
            ModValue = modValue;
        }

        ModResult IModFunction.PerformCheck(string accountNumber, WeightTableEntry weights)
        {
            var positionValues = GetPositionValues(accountNumber);

            var multiples = positionValues
                .Select(v => weights.ValueDictionary
                    .First(w => w.Key == v.Key)
                    .Value * v.Value
                );

            var sum = multiples.Sum();
            var remainder = sum % ModValue;
            
            var result = new ModResult
            {
                ResultSum = sum,
                Remainder = remainder,
                Passes = remainder == 0,
            };

            return result;
        }
    }
}
