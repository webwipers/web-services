﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Modulus.Processes.Functions.Implementations
{
    public abstract class BaseModFunction
    {
        private static readonly char[] positions = new[] { 'u', 'v', 'w', 'x', 'y', 'z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' };
        
        protected IDictionary<char, int> GetPositionValues(string numberString)
        {
            var lookup = new Dictionary<char, int>();

            for (int i = 0; i < positions.Length; ++i)
            {
                var intValue = (int)char.GetNumericValue(numberString[i]);
                lookup.Add(positions[i], intValue);
            }

            return lookup;
        }

        protected IEnumerable<int> GetIntsFromString(string numberString)
        {
           return numberString
                .ToCharArray()
                .Select(c => (int)char.GetNumericValue(c));
        }
    }
}
