﻿using Modulus.DataAccessLayer.Entities;
using Modulus.Processes.Entities;

namespace Modulus.Processes.Functions
{
    public interface IModFunction
    {
        ModResult PerformCheck(string accountNumber, WeightTableEntry weights);
    }
}
