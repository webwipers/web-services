﻿using Modulus.DataAccessLayer.Enums;
using Modulus.Processes.Functions.Implementations;
using System;

namespace Modulus.Processes.Functions
{
    public static class Factory
    {
        public static IModFunction GetModFunction(CheckType checkType)
        {
            switch(checkType)
            {
                case CheckType.MOD10:
                    return new StandardFunction(10);

                case CheckType.MOD11:
                    return new StandardFunction(11);

                case CheckType.DBLAL:
                    return new AlternateFunction();

                default:
                    throw new ArgumentOutOfRangeException($"Check type {checkType} is not valid. ");
            }
        }
    }
}
