﻿using Modulus.DataAccessLayer.Enums;
using System.Collections.Generic;

namespace Modulus.DataAccessLayer.Entities
{
    public sealed class WeightTableEntry
    {
        public CheckType ModCheck { get; set; }

        public int StartIndex { get; set; }

        public int EndIndex { get; set; }

        public int U => ValueDictionary['u'];

        public int V => ValueDictionary['v'];

        public int W => ValueDictionary['w'];

        public int X => ValueDictionary['x'];

        public int Y => ValueDictionary['y'];

        public int Z => ValueDictionary['z'];

        public int A => ValueDictionary['a'];

        public int B => ValueDictionary['b'];

        public int C => ValueDictionary['c'];

        public int D => ValueDictionary['d'];

        public int E => ValueDictionary['e'];

        public int F => ValueDictionary['f'];

        public int G => ValueDictionary['g'];

        public int H => ValueDictionary['h'];

        public int? Ex { get; set; }

        public IDictionary<char, int> ValueDictionary { get; set; }

    }
}
