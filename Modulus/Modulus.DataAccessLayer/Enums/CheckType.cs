﻿namespace Modulus.DataAccessLayer.Enums
{
    public enum CheckType
    {
        Unspecified = 0,
        MOD10 = 1,
        MOD11 = 2,
        DBLAL = 3,
    }
}
