﻿using System.Collections.Generic;
using Modulus.DataAccessLayer.Entities;

namespace Modulus.DataAccessLayer.DataAccessors
{
    public interface IFileDataAccessor
    {
        IEnumerable<WeightTableEntry> WeightTable { get; }
    }
}
