﻿using Modulus.DataAccessLayer.Entities;
using Modulus.DataAccessLayer.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Modulus.DataAccessLayer.DataAccessors.Implementations
{
    public class FileDataAccessor : IFileDataAccessor
    {
        private static readonly FieldSpecification StartIndexSpec = new FieldSpecification { Name = "StartIndex", Length = 6, StartIndex = 0};
        private static readonly FieldSpecification EndIndexSpec = new FieldSpecification { Name = "EndIndex", Length = 6, StartIndex = 7 };
        private static readonly FieldSpecification CheckSpec = new FieldSpecification { Name = "Check", Length = 5, StartIndex = 14};
        private static readonly FieldSpecification USpec = new FieldSpecification{ Name = "U", Length = 4, StartIndex = 20 };
        private static readonly FieldSpecification VSpec = new FieldSpecification{ Name = "V", Length = 4, StartIndex = 25 };
        private static readonly FieldSpecification WSpec = new FieldSpecification{ Name = "W", Length = 4, StartIndex = 30 };
        private static readonly FieldSpecification XSpec = new FieldSpecification{ Name = "X", Length = 4, StartIndex = 35 };
        private static readonly FieldSpecification YSpec = new FieldSpecification{ Name = "Y", Length = 4, StartIndex = 40 };
        private static readonly FieldSpecification ZSpec = new FieldSpecification{ Name = "Z", Length = 4, StartIndex = 45 };
        private static readonly FieldSpecification ASpec = new FieldSpecification{ Name = "A", Length = 4, StartIndex = 50 };
        private static readonly FieldSpecification BSpec = new FieldSpecification{ Name = "B", Length = 4, StartIndex = 55 };
        private static readonly FieldSpecification CSpec = new FieldSpecification{ Name = "C", Length = 4, StartIndex = 60 };
        private static readonly FieldSpecification DSpec = new FieldSpecification{ Name = "D", Length = 4, StartIndex = 65 };
        private static readonly FieldSpecification ESpec = new FieldSpecification{ Name = "E", Length = 4, StartIndex = 70 };
        private static readonly FieldSpecification FSpec = new FieldSpecification{ Name = "F", Length = 4, StartIndex = 75 };
        private static readonly FieldSpecification GSpec = new FieldSpecification{ Name = "G", Length = 4, StartIndex = 80 };
        private static readonly FieldSpecification HSpec = new FieldSpecification { Name = "H", Length = 4, StartIndex = 85 };
        private static readonly FieldSpecification ExSpec = new FieldSpecification { Name = "Ex", Length = 3, StartIndex = 90 };

        private readonly Lazy<IEnumerable<WeightTableEntry>> weightTable = new Lazy<IEnumerable<WeightTableEntry>>(ConstructWeightTable);

        IEnumerable<WeightTableEntry> IFileDataAccessor.WeightTable => weightTable.Value;

        private static IEnumerable<WeightTableEntry> ConstructWeightTable()
        {
            var lines = ReadFromFile();
            var entries = lines.Select(ConvertToWeightTableEntry);

            return entries;
        }

        private static IEnumerable<string> ReadFromFile()
        {
            var filePath = Path.Combine(Environment.CurrentDirectory, "DataFiles/valacdos.txt");
            var lines = File.ReadAllLines(filePath);

            return lines;
        }

        private static WeightTableEntry ConvertToWeightTableEntry(string fileLine)
        {
            var indexedWeights = new Dictionary<char, int>
            {
                {'u', USpec.ExtractIntValue(fileLine) },
                {'v', VSpec.ExtractIntValue(fileLine) },
                {'w', WSpec.ExtractIntValue(fileLine) },
                {'x', XSpec.ExtractIntValue(fileLine) },
                {'y', YSpec.ExtractIntValue(fileLine) },
                {'z', ZSpec.ExtractIntValue(fileLine) },
                {'a', ASpec.ExtractIntValue(fileLine) },
                {'b', BSpec.ExtractIntValue(fileLine) },
                {'c', CSpec.ExtractIntValue(fileLine) },
                {'d', DSpec.ExtractIntValue(fileLine) },
                {'e', ESpec.ExtractIntValue(fileLine) },
                {'f', FSpec.ExtractIntValue(fileLine) },
                {'g', GSpec.ExtractIntValue(fileLine) },
                {'h', HSpec.ExtractIntValue(fileLine) },
            };

            var entry = new WeightTableEntry
            {
                StartIndex = StartIndexSpec.ExtractIntValue(fileLine),
                EndIndex = EndIndexSpec.ExtractIntValue(fileLine),
                ModCheck = Enum.Parse<CheckType>(CheckSpec.ExtractStringValue(fileLine)),
                ValueDictionary = indexedWeights,
            };


            if (fileLine.Length > 90)
            {
                entry.Ex = ExSpec.ExtractIntValue(fileLine);
            }
            return entry;
        }
    }

    internal class FieldSpecification 
    {
        public string Name { get; set; }

        public int StartIndex { get; set; }

        public int Length { get; set; }

        public int ExtractIntValue(string fileLine)
        {
            var substring = ExtractStringValue(fileLine);

            if(string.IsNullOrWhiteSpace(substring))
            {
                return default(int);
            }

            return int.Parse(substring);
        }

        public string ExtractStringValue(string fileLine)
        {
            return fileLine.Substring(StartIndex, Length); ;
        }
    }
}
