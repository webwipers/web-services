﻿using Modulus.DataAccessLayer.Entities;
using System.Collections.Generic;

namespace Modulus.DataAccessLayer.Repositories
{
    public interface IWeightTableRepository
    {
        IEnumerable<WeightTableEntry> GetWeightsForSortCode(string sortCode);
        
    }
}
