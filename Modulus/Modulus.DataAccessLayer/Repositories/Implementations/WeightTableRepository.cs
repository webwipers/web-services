﻿using Modulus.DataAccessLayer.DataAccessors;
using Modulus.DataAccessLayer.DataAccessors.Implementations;
using Modulus.DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Modulus.DataAccessLayer.Repositories.Implementations
{
    public sealed class WeightTableRepository : IWeightTableRepository
    {
        private readonly IFileDataAccessor _fileDataAccessor = new FileDataAccessor();

        IEnumerable<WeightTableEntry> IWeightTableRepository.GetWeightsForSortCode(string sortCode)
        {
            if(!int.TryParse(sortCode, out var sortCodeInt))
            {
                throw new ApplicationException("This should have been validated as an intiger before now. ");
            }

            return _fileDataAccessor.WeightTable.Where(w => w.StartIndex <= sortCodeInt && w.EndIndex >= sortCodeInt);
        }
    }
}
